import express from 'express'
import data from '../src/songs';
const router = express.Router();

router.get('/songs',(req, res) => {
    res.send({
        songs: data.songs
    });
});

export default router;
