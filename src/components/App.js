import React from 'react';
import Header from './Header';
import ListPreview from './ListPreview';
import axios from 'axios';

class App extends React.Component {
    state = {
        pageHeader: "Hangar O'clock Songs",
        songs:[]
    };
    componentDidMount(){
      //it consumes the data once react is ready
      axios.get('/api/songs')
      .then(resp => {
        this.setState({
          songs: resp.data.songs
        });
      })
      .catch(console.error);
    }



    componentWillunmount(){
        // clean timers, listeners etc...
    }

    render(){
      return(
            <div className= "App">
              <Header message={this.state.pageHeader} />
            <div>
                {this.state.songs.map(song =>
                  <ListPreview key={song.id}{...song} />
                )}
            </div>
        </div>
    );
  }
}
export default App;
