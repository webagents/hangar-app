import React from 'react';

const ListPreview = (song) => (
<div className="ListPreview">

    <tr>
      <th className="song-name">
        {song.songname}
      </th>
      <th className="artist-name">
        {song.artistname}
      </th>
      <td> <button className = "btn btn-primary"> Today </button>
      <button className = "btn btn-primary"> Friday </button> </td>
      </tr>
</div>
);

export default ListPreview;
